#!/bin/bash

set -ex

RESULTS_DIR="/mnt/results"

/mnt/testing/build-mesa.sh
/mnt/testing/check-device.sh

if [ "$RUN_CTS" -eq 1 ]; then
    echo "Running CTS..."

    pushd /mnt/mesa

    export FDO_CI_CONCURRENT=$NUM_THREADS
    export GPU_VERSION="radv-"$EXPECTED_GPU_FAMILY"-aco"
    export DEQP_RESULTS_DIR="../.."$RESULTS_DIR"/cts"
    export DRIVER_NAME="radv"
    export DEQP_VER="vk"

    # Copy the skips/fails/flakes lists.
    cp .gitlab-ci/all-skips.txt install/
    find src/amd/ci/ -name "*.txt" -exec cp {} install \;

    set +e

    /mnt/testing/deqp-runner.sh

    rm $RESULTS_DIR/cts/*.shader_cache
    popd
fi

if [ "$RUN_VKD3D_PROTON" -eq 1 ]; then
    echo "Running vkd3d-proton testsuite..."

    set +ex
    export LD_LIBRARY_PATH=/mnt/mesa/install/lib/:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu:/vkd3d-proton-tests/x64
    export VK_ICD_FILENAMES=/mnt/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

    /vkd3d-proton-tests/x64/bin/d3d12 &> $RESULTS_DIR/vkd3d-proton.log

    grep "tests execute" $RESULTS_DIR/vkd3d-proton.log | grep -v "0 failures"
    if [ $? == 0 ]; then
        echo "Regressions found, see vkd3d-proton.log!"
        exit 1
    fi
    echo "No vkd3d-proton regressions found!"
fi

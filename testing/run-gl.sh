#!/bin/bash

set -ex

RESULTS_DIR="/mnt/results"

/mnt/testing/build-mesa.sh
/mnt/testing/check-device.sh

pushd /mnt/mesa

set +e

export FDO_CI_CONCURRENT=$NUM_THREADS

# Copy the skips/fails/flakes lists.
cp .gitlab-ci/all-skips.txt install/
if [ "$GALLIUM_DRIVERS" == "zink" ]; then
    cp src/gallium/drivers/zink/ci/*radv* install/
fi

export DEQP_RESULTS_DIR="../.."$RESULTS_DIR"/deqp"
/mnt/testing/deqp-runner.sh

popd

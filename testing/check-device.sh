#!/bin/bash

export LD_LIBRARY_PATH=/mnt/mesa/install/lib/:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu
export VK_ICD_FILENAMES=/mnt/mesa/install/share/vulkan/icd.d/"$VK_DRIVER"_icd.x86_64.json

pushd /deqp/external/vulkancts/modules/vulkan
./deqp-vk --deqp-case=dEQP-VK.info.device &> /dev/null
GPU_FAMILY=`sed -rn 's/^deviceName: .* \(RADV ([0-9A-Z]+).*\)$/\1/p' TestResults.qpa | tr '[:upper:]' '[:lower:]'`
popd

if ! echo $GPU_FAMILY | grep -q $EXPECTED_GPU_FAMILY; then
    echo "Expected GPU is $EXPECTED_GPU_FAMILY, got $GPU_FAMILY."
    exit 1
fi

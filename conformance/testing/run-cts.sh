#!/bin/bash

set -ex

/mnt/testing/build-mesa.sh

# If the conformance CTS run is against a specific Mesa commit, use it for the
# package, otherwise grab the current one.
if [[ -z "${MESA_SOURCE_BRANCH}" ]]; then
    MESA_SHA1=$MESA_SOURCE_COMMIT
else
    pushd /mnt/mesa
    MESA_SHA1=$(git rev-parse --short HEAD)
    popd
fi

PKG_NAME="RADV_$GPU_FAMILY"
SUBMISSION_PKG_DIR=/mnt/results/${PKG_NAME}
mkdir -p $SUBMISSION_PKG_DIR

# Get list of products from chip.
case $GPU_FAMILY in
    navi2x)
        declare -a products=("Radeon RX 6900 XT"
                             "Radeon RX 6800 XT"
                             "Radeon RX 6800"
                             "Radeon RX 6700 XT"
                             "Radeon RX 6600 XT"
                             "Radeon RX 6600"
                             "AMD Custom GPU 0405")
        ;;
    navi1x)
        declare -a products=("Radeon RX 5700 XT"
                             "Radeon RX 5700"
                             "Radeon RX 5600 XT"
                             "Radeon RX 5600"
                             "Radeon RX 5500 XT"
                             "Radeon RX 5500"
                             "Radeon RX 5500M")
        ;;
    vega)
        declare -a products=("Radeon Vega Frontier Edition"
                             "Radeon RX Vega64"
                             "Radeon RX Vega56"
                             "Radeon VII")
        ;;
    polaris)
        declare -a products=("Radeon RX 460"
                             "Radeon RX 470"
                             "Radeon RX 480"
                             "Radeon RX 550"
                             "Radeon RX 560"
                             "Radeon RX 570"
                             "Radeon RX 580"
                             "Radeon RX 590")
        ;;
    # TODO: Add GFX6 and GFX7 products.
    *)
        echo "Unsupported chip name!"
        exit 1
esac

cd /CTS/build-m64/external/vulkancts/modules/vulkan/

# Maximum allowed for a CTS conformance submission.
N=8

# Run CTS.
# See https://github.com/KhronosGroup/VK-GL-CTS/blob/main/external/vulkancts/README.md#running-cts

# 64-bit run.
cd /CTS/build-m64/external/vulkancts/modules/vulkan/
for I in {0..7}; do
    echo "Starting (64-bit) deqp-vk ($I) ..."
    VK_ICD_FILENAMES=/mesa/install64/share/vulkan/icd.d/radeon_icd.x86_64.json ./deqp-vk \
        --deqp-log-images=disable \
        --deqp-log-shader-sources=disable \
        --deqp-log-flush=disable \
        --deqp-caselist-file=/VK-GL-CTS/external/vulkancts/mustpass/main/vk-default.txt \
        --deqp-fraction-mandatory-caselist-file=/VK-GL-CTS/external/vulkancts/mustpass/main/vk-fraction-mandatory-tests.txt \
        --deqp-log-filename=${SUBMISSION_PKG_DIR}/TestResults-${PKG_NAME}-Linux-x86_64-${I}-of-${N}.qpa \
        --deqp-shadercache-filename=shadercache-${I}.bin \
        --deqp-fraction=${I},${N} >/dev/null &
done

# 32-bit run.
cd /CTS/build-m32/external/vulkancts/modules/vulkan/
for I in {0..7}; do
    echo "Starting (32-bit) deqp-vk ($I) ..."
    VK_ICD_FILENAMES=/mesa/install32/share/vulkan/icd.d/radeon_icd.i686.json ./deqp-vk \
        --deqp-log-images=disable \
        --deqp-log-shader-sources=disable \
        --deqp-log-flush=disable \
        --deqp-caselist-file=/VK-GL-CTS/external/vulkancts/mustpass/main/vk-default.txt \
        --deqp-fraction-mandatory-caselist-file=/VK-GL-CTS/external/vulkancts/mustpass/main/vk-fraction-mandatory-tests.txt \
        --deqp-log-filename=${SUBMISSION_PKG_DIR}/TestResults-${PKG_NAME}-Linux-x86-${I}-of-${N}.qpa \
        --deqp-shadercache-filename=shadercache-${I}.bin \
        --deqp-fraction=${I},${N} >/dev/null &
done

# Wait for all instances.
wait

# Create the conformance submission package.
# See https://github.com/KhronosGroup/VK-GL-CTS/blob/main/external/vulkancts/README.md#conformance-submission-package-requirements
cd /VK-GL-CTS

# 2. Result of git status and git log from CTS source directory
git status > $SUBMISSION_PKG_DIR/git-status.txt
git log --first-parent $RELEASE_TAG^..HEAD > $SUBMISSION_PKG_DIR/git-log.txt

# 3. Any patches used on top of release tag
git format-patch -o $SUBMISSION_PKG_DIR $RELEASE_TAG..HEAD

# 4. Conformance statement
STATEMENT_FILE=$SUBMISSION_PKG_DIR/STATEMENT-SPI
echo "CONFORM_VERSION: $RELEASE_TAG" > $STATEMENT_FILE
echo "Driver: RADV (Mesa git-$MESA_SHA1)" >> $STATEMENT_FILE
for product in "${products[@]}"
do
   echo "PRODUCT: $product" >> $STATEMENT_FILE
done
echo "CPU: $(cat /proc/cpuinfo | grep 'model name' | cut -d ':' -f2 | sed -e 's/^ //' | uniq)" >> $STATEMENT_FILE
echo "OS: $(uname -s -r)" >> $STATEMENT_FILE

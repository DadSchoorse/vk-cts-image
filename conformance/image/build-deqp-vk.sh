#!/bin/bash

set -ex

# https://github.com/KhronosGroup/VK-GL-CTS/blob/master/external/vulkancts/README.md

CTS_VERSION="vulkan-cts-1.3.5.1"

git clone https://github.com/KhronosGroup/VK-GL-CTS /VK-GL-CTS
pushd /VK-GL-CTS

git checkout $CTS_VERSION

# Fetch submodules.
python3 external/fetch_sources.py --insecure

# 64-bit version
mkdir -p /CTS/build-m64
cmake \
    -B /CTS/build-m64 \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_FLAGS="-m64" \
    -DCMAKE_CXX_FLAGS="-m64"
ninja -C /CTS/build-m64

# 32-bit version
mkdir -p /CTS/build-m32
cmake \
    -B /CTS/build-m32 \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_FLAGS="-m32 -msse2 -mfpmath=sse" \
    -DCMAKE_CXX_FLAGS="-m32 -msse2 -mfpmath=sse"
ninja -C /CTS/build-m32

popd

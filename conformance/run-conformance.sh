#!/bin/bash

TEST_IMAGE=$(cat IMAGE_VERSION)
RESULT_DIR=`pwd`/results
VK_VERSION="13"

gpu_device_id=0
gpu_families=(polaris
              vega
              navi1x
              navi2x)

while getopts "hd:g:" OPTION; do
    case $OPTION in
    h)
        echo -n "$0 [ -d <gpu_device> ]"
        exit 0
        ;;
    d)
        gpu_device_id=$OPTARG
        ;;
    g)
        gpu_family=$OPTARG
        ;;
    *)
        if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
            echo "Non-option argument: '-${OPTARG}'" >&2
        fi
        exit 1
        ;;
    esac
done

found=0
for g in "${gpu_families[@]}"
do
    if [ "$g" == "$gpu_family" ] ; then
        found=1
    fi
done

if [ $found -eq 0 ]; then
    echo "Unsupported GPU family option (accepted values: ${gpu_families[@]})"
    exit 1
fi

set -ex

# Create the results dir if it doesn't exist.
mkdir -p $RESULT_DIR

# Adjust the permissions of the X server host for docker.
xhost +local:root || exit 1

# Pull the image.
docker pull $TEST_IMAGE

# Run the image.
docker run \
    --device /dev/dri/renderD$((128+$gpu_device_id)) \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/../external/mesa,target=/mnt/mesa,type=bind \
    --mount src=$RESULT_DIR,target=/mnt/results,type=bind \
    --network host \
    --security-opt label=disable \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    --env DISPLAY=$DISPLAY \
    --env RELEASE_TAG="vulkan-cts-1.3.5.1" \
    --env MESA_SOURCE_REMOTE="origin" \
    --env MESA_SOURCE_COMMIT="1311eddd526" \
    --env GPU_FAMILY=$gpu_family \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-cts.sh

# Generate the tarball.
echo "Generating CTS tarball..."
pkg_dir="RADV_$gpu_family"
pkg_name="VK"$VK_VERSION"_SPI_"$pkg_dir"_Linux.tgz"
tar -czvf $RESULT_DIR/$pkg_name -C $RESULT_DIR/$pkg_dir .

echo "Please check if the tarball is conformant with ./check-tarball.sh!"
echo "Thanks!"

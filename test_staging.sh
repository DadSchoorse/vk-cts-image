#!/bin/bash

# Example: ./test_staging.sh 21.0 navi10
if [ $# -lt 2 ]; then
    echo "$0 <version> <gpu_family> [<device_id>]"
    exit 1
fi

version=$1
gpu_family=$2
device_id=0

if [ $# == 3 ]; then
    device_id=$3
fi

./run-radv.sh -r origin -b staging/$version -g $gpu_family -d $device_id --no-rebase
./run-fossilize.sh -r origin -b staging/$version -d $device_id --no-rebase
